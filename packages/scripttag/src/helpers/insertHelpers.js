export const insertAfter = (el, referenceNode) => {
  referenceNode.parentNode.insertBefore(el, referenceNode.nextSibling);
};

export const insertBefore = (el, referenceNode) => {
  referenceNode.parentNode.insertBefore(el, referenceNode);
};

export const insertInside = (el, referenceNode) => {
  referenceNode.parentNode.appendChild(el);
};

export function truncateString(str, num) {
  if (str.length > num) {
    return str.slice(0, num) + '...';
  } else {
    return str;
  }
}
