import React from 'react';
import './NotificationPopup.scss';
import {truncateString} from '../../helpers/insertHelpers';
import PropTypes from 'prop-types';

export const NotificationPopup = ({
  firstName = 'John Doe',
  city = 'New York',
  country = 'United States',
  productName = 'Puffer Jacket With Hidden Hood',
  relativeDate = 'a day ago',
  productImage = 'https://picsum.photos/65',
  productLink,
  settings = {
    hideTimeAgo: false,
    truncateProductName: false,
    position: 'top-right'
  }
}) => {
  const {hideTimeAgo, truncateProductName, position} = settings;
  let arrPosition = position.split('-');
  let stylePosition = {};
  arrPosition[0] === 'top'
    ? (stylePosition = {...stylePosition, top: '15px'})
    : (stylePosition = {...stylePosition, bottom: '15px'});
  arrPosition[1] === 'right'
    ? (stylePosition = {...stylePosition, right: '15px'})
    : (stylePosition = {...stylePosition, left: '15px'});
  return (
    <div style={stylePosition} className="Avava-SP__Wrapper fadeInUp animated">
      <div className="Avava-SP__Inner">
        <div className="Avava-SP__Container">
          <a href={productLink} className={'Avava-SP__LinkWrapper'}>
            <div
              className="Avava-SP__Image"
              style={{
                backgroundImage: `url(${productImage})`
              }}
            ></div>
            <div className="Avada-SP__Content">
              <div className={'Avada-SP__Title'}>
                {firstName} in {city}, {country}
              </div>
              <div className={'Avada-SP__Subtitle'}>
                purchased{' '}
                {truncateProductName
                  ? truncateString(productName, 16)
                  : productName}
              </div>
              <div className={'Avada-SP__Footer'}>
                {hideTimeAgo ? '' : `${relativeDate}`}
                <span className="uni-blue">
                  <i className="fa fa-check" aria-hidden="true" /> ✔ by Avada
                </span>
              </div>
            </div>
          </a>
        </div>
      </div>
    </div>
  );
};

NotificationPopup.propTypes = {
  firstName: PropTypes.string,
  city: PropTypes.string,
  country: PropTypes.string,
  productName: PropTypes.string,
  timestamp: PropTypes.any,
  settings: PropTypes.object,
  productImage: PropTypes.string
};
