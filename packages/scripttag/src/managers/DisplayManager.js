import {insertAfter} from '../helpers/insertHelpers';
import {render} from 'preact';
import React from 'preact/compat';
import {NotificationPopup} from '../components/NotificationPopup/NotificationPopup';
import {ALLOW_SHOW_ALL_PAGE, SPECIFIC_SHOW_PAGE} from '../config';

export default class DisplayManager {
  constructor() {
    this.notifications = [];
    this.settings = {};
  }
  /**
   *
   * @param {*} ms
   * @returns
   */
  delay = ms => new Promise(res => setTimeout(res, ms));
  /**
   *
   * @param {*} param0
   */
  async initialize({notifications, settings}) {
    const url = location.href;
    // Todo: With notification and settings, update the displaying logic
    if (
      settings.allowShow === ALLOW_SHOW_ALL_PAGE &&
      settings.excludedUrls.includes(url)
    ) {
      return;
    }
    if (
      settings.allowShow === SPECIFIC_SHOW_PAGE &&
      !settings.includedUrls.includes(url)
    ) {
      return;
    }

    var count = settings.maxPopsDisplay;
    this.insertContainer();
    await this.delay(settings.firstDelay * 1000);

    for (const notification of notifications.slice(0, count)) {
      await this.displayOnePopup(notification, settings);
    }
    this.removeContainer();
  }

  /**
   *
   * @param {*} notification
   * @param {*} settings
   */
  async displayOnePopup(notification, settings) {
    this.display({notification, settings});
    await this.delay(settings.displayDuration * 1000);
    this.fadeOut();
    await this.delay(settings.popsInterval * 1000);
  }
  /**
   *
   */

  fadeOut() {
    const container = document.querySelector('#Avada-SalePop');

    container.innerHTML = '';
  }
  /**
   *
   * @param {*} param0
   */

  display({notification, settings}) {
    const container = document.querySelector('#Avada-SalePop');
    render(
      <NotificationPopup
        {...notification}
        settings={{
          hideTimeAgo: settings.hideTimeAgo,
          truncateProductName: settings.truncateProductName,
          position: settings.position
        }}
      />,
      container
    );
  }
  /**
   *
   * @returns
   */

  insertContainer() {
    const popupEl = document.createElement('div');
    popupEl.id = `Avada-SalePop`;
    popupEl.classList.add('Avada-SalePop__OuterWrapper');
    const targetEl = document.querySelector('body').firstChild;
    if (targetEl) {
      insertAfter(popupEl, targetEl);
    }

    return popupEl;
  }

  /**
   *
   */
  removeContainer() {
    const container = document.querySelector('#Avada-SalePop');
    container.remove();
    return;
  }
}
