import makeRequest from '../helpers/api/makeRequest';

export default class ApiManager {
  getNotifications = async () => {
    return this.getApiData();
  };

  getApiData = async () => {
    const {host} = window.location;
    const apiUrl = `https://localhost:3000/clientApi/notifications?shopifyDomain=${host}`;
    const {notifications, settings} = await makeRequest(apiUrl);

    return {notifications, settings};
  };
}
