import React, {useState} from 'react';
import NotificationPopup from '../NotificationPopup';
import {Card, ResourceItem, ResourceList, TextStyle} from '@shopify/polaris';
import useFetchApi from '../../hooks/useFetchApi';
import moment from 'moment';
import {api} from '../../helpers';

export default function NotificationList() {
  const [sortValue, setSortValue] = useState('DATE_MODIFIED_DESC');

  const resourceName = {
    singular: 'notification',
    plural: 'notifications'
  };

  const {data: items, loading, setData, setLoading} = useFetchApi(
    '/notifications?page=1'
  );

  async function sortFunction(sortValue) {
    if (sortValue === 'DATE_MODIFIED_DESC') {
      setLoading(true);
      const resp = await api('/notifications?sort=desc&page=1');
      const newItems = resp.data;
      setData(newItems);
      setLoading(false);
    }
    if (sortValue === 'DATE_MODIFIED_ASC') {
      setLoading(true);
      const resp = await api('/notifications?sort=asc&page=1');
      const newItems = resp.data;
      setData(newItems);
      setLoading(false);
    }
  }

  return (
    <Card>
      <ResourceList
        resourceName={resourceName}
        items={items}
        renderItem={renderItem}
        sortValue={sortValue}
        loading={loading}
        sortOptions={[
          {label: 'Newest update', value: 'DATE_MODIFIED_DESC'},
          {label: 'Oldest update', value: 'DATE_MODIFIED_ASC'}
        ]}
        onSortChange={selected => {
          setSortValue(selected);
          sortFunction(selected);
        }}
      />
    </Card>
  );

  function renderItem(item) {
    const {
      firstName,
      city,
      productName,
      country,
      productId,
      timestamp,
      productImage
    } = item;
    const day = moment(timestamp).format('MMMM D');
    const year = moment(timestamp).format('YYYY');

    return (
      <ResourceItem id={productId}>
        <div className="item-noti">
          <NotificationPopup
            firstName={firstName}
            city={city}
            country={country}
            productName={productName}
            timestamp={timestamp}
            productImage={productImage}
          />
          <TextStyle>
            <p>From {day}</p>
            <p style={{textAlign: 'right'}}>{year}</p>
          </TextStyle>
        </div>
      </ResourceItem>
    );
  }
}
