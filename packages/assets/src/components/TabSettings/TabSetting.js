import React, {useCallback, useState} from 'react';
import {Card, Tabs} from '@shopify/polaris';
import DesktopPositionInput from '../DesktopPositionInput';
import './Setting.scss';
import NotificationControl from '../NotificationControl';
import PageRestriction from '../PageRestriction';

export default function TabSetting({handleChangeInput, input}) {
  const [selected, setSelected] = useState(0);

  const handleTabChange = useCallback(
    selectedTabIndex => setSelected(selectedTabIndex),
    []
  );

  const tabs = [
    {
      id: 'display',
      content: 'Display',
      name: 'APPEARANCE'
    },
    {
      id: 'triggers',
      content: 'Triggers',
      name: 'PAGES RESTRICTION'
    }
  ];
  return (
    <Card>
      <Tabs tabs={tabs} selected={selected} onSelect={handleTabChange}>
        <Card.Section title={tabs[selected].name}>
          <div className="tabs-contents">
            {tabs[selected].id === 'display' ? (
              <React.Fragment>
                <DesktopPositionInput
                  label="Desktop Position"
                  onChange={val => handleChangeInput('position', val)}
                  value={input.position}
                  helpText="The display position of the pop on your website"
                />
                <NotificationControl
                  input={input}
                  handleChangeInput={handleChangeInput}
                />
              </React.Fragment>
            ) : (
              <PageRestriction
                input={input}
                handleChangeInput={handleChangeInput}
              />
            )}
          </div>
        </Card.Section>
      </Tabs>
    </Card>
  );
}
