import React from 'react';
import {RangeSlider, Stack, TextField} from '@shopify/polaris';
import './NotificationSetting.scss';

export default function NotificationSetting({input, handleChangeInput}) {
  return (
    <div className="notification-setting">
      <h1>TIMING</h1>
      <div className="setting-content">
        <Stack distribution="fillEvenly">
          <Stack.Item>
            <div className="setting-item">
              <RangeSlider
                label="Display Duration"
                value={input.displayDuration}
                onChange={val => {
                  handleChangeInput('displayDuration', val);
                }}
                output
                min={1}
                suffix={
                  <div className="btn-value" style={{width: '120px'}}>
                    <TextField
                      prefix={input.displayDuration}
                      suffix="Second (s)"
                      disabled
                    />
                  </div>
                }
                helpText="How long each pop will display on your page"
              />
            </div>
          </Stack.Item>
          <Stack.Item>
            <div className="setting-item" style={{display: 'inline'}}>
              <RangeSlider
                label="Time before the first pop"
                value={input.firstDelay}
                onChange={val => {
                  handleChangeInput('firstDelay', val);
                }}
                output
                min={1}
                suffix={
                  <div className="btn-value" style={{width: '120px'}}>
                    <TextField
                      prefix={input.firstDelay}
                      suffix="Second (s)"
                      disabled
                    />
                  </div>
                }
                helpText="The delay time before the first notification"
              />
            </div>
          </Stack.Item>
        </Stack>
      </div>

      <div className="setting-content row">
        <Stack distribution="fillEvenly">
          <Stack.Item>
            <div className="setting-item" style={{display: 'inline'}}>
              <RangeSlider
                label="Gap time between tow pop"
                value={input.popsInterval}
                onChange={val => {
                  handleChangeInput('popsInterval', val);
                }}
                output
                min={1}
                suffix={
                  <div className="btn-value" style={{width: '120px'}}>
                    <TextField
                      prefix={input.popsInterval}
                      suffix="Second (s)"
                      disabled
                    />
                  </div>
                }
                helpText="The time interval between two popup notification"
              />
            </div>
          </Stack.Item>
          <Stack.Item>
            <div className="setting-item" style={{display: 'inline'}}>
              <RangeSlider
                label="Maximum of popups"
                max={80}
                min={1}
                value={input.maxPopsDisplay}
                onChange={val => {
                  handleChangeInput('maxPopsDisplay', val);
                }}
                output
                suffix={
                  <div className="btn-value" style={{width: '120px'}}>
                    <TextField
                      prefix={input.maxPopsDisplay}
                      suffix="pop (s)"
                      disabled
                    />
                  </div>
                }
                helpText={
                  <p>
                    The maximum number of the popups are allowed to show after
                    <br></br> page loading. Maximum number is 80
                  </p>
                }
              />
            </div>
          </Stack.Item>
        </Stack>
      </div>
    </div>
  );
}
