import React from 'react';
import {FormLayout, Select, TextField, TextContainer} from '@shopify/polaris';
import './PageRestriction.scss';

export default function PageRestriction({input, handleChangeInput}) {
  const options = [
    {label: 'All pages', value: 'all'},
    {label: 'Specific pages', value: 'specific'}
  ];

  return (
    <FormLayout>
      <FormLayout>
        <Select
          options={options}
          onChange={val => {
            handleChangeInput('allowShow', val);
          }}
          value={input.allowShow}
        />
        {input.allowShow === 'all' ? (
          <TextField
            label="Excluded pages"
            multiline={4}
            helpText="Page URLs NOT to show the pop-up (separated ny new lines)"
            value={input.excludedUrls}
            onChange={val => {
              handleChangeInput('excludedUrls', val);
            }}
          />
        ) : (
          <TextContainer>
            <TextField
              label="Included pages"
              multiline={4}
              helpText="Page URLs to show the pop-up (separated ny new lines)"
              value={input.includedUrls}
              onChange={val => {
                handleChangeInput('includedUrls', val);
              }}
            />
          </TextContainer>
        )}
      </FormLayout>
    </FormLayout>
  );
}
