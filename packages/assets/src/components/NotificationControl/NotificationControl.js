import React from 'react';
import {Checkbox} from '@shopify/polaris';
import './NotificationControl.scss';
import NotificationSetting from '../NotificationSetting';

export default function NotificationControl({input, handleChangeInput}) {
  return (
    <div className="check-list-position-control">
      <Checkbox
        label="Hide Time Ago"
        checked={input.hideTimeAgo}
        onChange={val => {
          handleChangeInput('hideTimeAgo', val);
        }}
      />
      <Checkbox
        label="Truncate content text"
        helpText={`If your product name is long for the one line , it will be truncated to 'Product na...'`}
        onChange={val => {
          handleChangeInput('truncateProductName', val);
        }}
        checked={input.truncateProductName}
      />
      <NotificationSetting
        input={input}
        handleChangeInput={handleChangeInput}
      />
    </div>
  );
}
