import {
  Page,
  Layout,
  SkeletonBodyText,
  Card,
  TextContainer,
  SkeletonDisplayText
} from '@shopify/polaris';
import React, {useState} from 'react';
import {store} from '../..';
import {setToast} from '../../actions/layout/setToastAction';
import NotificationPopup from '../../components/NotificationPopup';
import TabSetting from '../../components/TabSettings';
import {api} from '../../helpers';
import useFetchApi from '../../hooks/useFetchApi';

export default function Settings() {
  const {loading, data: input, setData: setInput} = useFetchApi('/settings');
  const [updated, setUpdated] = useState(false);

  const [loadingUpdated, setLoadingUpdated] = useState(false);

  const handleChangeInput = (key, value) => {
    setInput(prevInput => ({
      ...prevInput,
      [key]: value
    }));
  };

  async function updateData(input) {
    setLoadingUpdated(true);
    await api('/settings', 'PUT', input);
    setLoadingUpdated(false);
    setUpdated(true);
    store.dispatch(
      setToast({
        content: 'Settings Updated'
      })
    );
  }

  const handleSubmit = e => {
    updateData(input);
  };

  return loading ? (
    <Page
      title="Settings"
      subtitle="Decide how your notifications will display"
      primaryAction={{content: 'Save', onAction: handleSubmit}}
      fullWidth
    >
      <Layout>
        <Layout.Section secondary>
          <Card sectioned>
            <SkeletonBodyText />
          </Card>
        </Layout.Section>
        <Layout.Section>
          <Card subdued>
            <Card.Section>
              <TextContainer>
                <SkeletonDisplayText size="large" />
                <SkeletonBodyText lines={2} />
              </TextContainer>
            </Card.Section>
            <Card.Section>
              <SkeletonBodyText lines={2} />
            </Card.Section>
          </Card>
        </Layout.Section>
      </Layout>
    </Page>
  ) : (
    <Page
      title="Settings"
      subtitle="Decide how your notifications will display"
      primaryAction={{
        content: 'Save',
        onAction: handleSubmit,
        loading: loadingUpdated
      }}
      fullWidth
    >
      <Layout>
        <Layout.Section secondary>
          <NotificationPopup
            settings={{
              hideTimeAgo: input.hideTimeAgo,
              truncateProductName: input.truncateProductName
            }}
          />
        </Layout.Section>
        <Layout.Section>
          <TabSetting handleChangeInput={handleChangeInput} input={input} />
        </Layout.Section>
      </Layout>
    </Page>
  );
}
