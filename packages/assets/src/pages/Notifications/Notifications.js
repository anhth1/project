import {Layout, Page, Pagination} from '@shopify/polaris';
import React from 'react';
import NotificationList from '../../components/NotificationList/index';
import './Notification.scss';

export default function Notifications() {
  return (
    <Page
      fullWidth
      title="Notifications"
      subtitle="List of sales notification from Shopify"
    >
      <Layout>
        <Layout.Section>
          <NotificationList />
        </Layout.Section>
        <Layout.Section>
          <div className="notification-pagination">
            <Pagination hasNext hasPrevious />
          </div>
        </Layout.Section>
      </Layout>
    </Page>
  );
}
