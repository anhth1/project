import React, {useState} from 'react';
import {
  Page,
  Layout,
  SkeletonBodyText,
  Card,
  SettingToggle,
  TextStyle
} from '@shopify/polaris';
import './Home.scss';
import {api} from '../../helpers';
import useFetchApi from '../../hooks/useFetchApi';
import {store} from '../..';
import {setToast} from '../../actions/layout/setToastAction';

/**
 * Render a home page for overview
 *
 * @return {React.ReactElement}
 * @constructor
 */
export default function Home() {
  const {loading, data: input, setData: setInput} = useFetchApi('/settings');
  const [loadingUpdated, setLoadingUpdated] = useState(false);

  const handleChangeInput = async (key, value) => {
    setInput(prevInput => ({
      ...prevInput,
      [key]: value
    }));
  };
  async function handleUpdateData() {
    setLoadingUpdated(true);
    handleChangeInput('active', !input.active);
    await api('/settings', 'PUT', {...input, active: !input.active});
    setLoadingUpdated(false);

    store.dispatch(
      setToast({
        content: `App ${contentStatus}`
      })
    );
  }

  const contentStatus = input.active ? 'Disable' : 'Enable';
  const textStatus = input.active ? 'enable' : 'disable';
  return loading ? (
    <Page title="Home" fullWidth>
      <Layout>
        <Layout.Section secondary>
          <Card sectioned>
            <SkeletonBodyText />
          </Card>
        </Layout.Section>
      </Layout>
    </Page>
  ) : (
    <Page title="Home" fullWidth>
      <SettingToggle
        action={{
          content: contentStatus,
          onAction: handleUpdateData,
          loading: loadingUpdated
        }}
        enabled={input.active}
      >
        App status is <TextStyle variation="strong">{textStatus}</TextStyle>.
      </SettingToggle>
    </Page>
  );
}
