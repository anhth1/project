import {useState, useEffect} from 'react';
import {api} from '../helpers';

export default function useFetchApi(
  url,
  defaultData = [],
  presentDataFunc = null,
  initialLoad = true
) {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState(defaultData);
  const [fetched, setFetched] = useState(false);

  async function fetchApi() {
    setLoading(true);
    try {
      const resp = await api(url);
      if (resp.data) {
        setData(resp.data);
      }
      setFetched(true);
      setLoading(false);
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  }
  useEffect(() => {
    fetchApi();
  }, []);

  return {loading, data, setData, fetched, setFetched, setLoading};
}
