import React from 'react';
import {Router} from 'react-router-dom';
import ReactRouterLink from './components/Link';
import {AppProvider} from '@shopify/polaris';
import translations from '@shopify/polaris/locales/en.json';
import {history} from './helpers';
import AppLayout from './components/AppLayout';
import ErrorBoundary from './components/ErrorBoundary';
import Routes from './routes/routes';

const theme = {
  colors: {
    topBar: {
      background: '#0b4697'
    }
  },
  logo: {
    width: 124,
    topBarSource:
      'https://lh3.googleusercontent.com/fife/AAWUweWJ-DlDaMFgJU9WodToW2iqLSKWkndBwy84vbtzVC5MZXZeZCXQ_3cxs8XvtTiD0Xs3P-tSENdFCd0yw0biy0_5PsZEANIQxkVdP8wT3d40nhxiu4ArZKkSdjXQKrWl8mVAHSPijoPsP_fJfe8IgRJBris8E_MU3hof0KwITh5SHLV5G9J8yzskeQliyPrdh4ThntCXj6jLF3iueqUIPljHNXqHr66tvve6X7a5GYd5dyTTdpqIrO6SsJT4z8RwGYUi3e-I0Weid32zh3Z95_ARhx1V_csgC75WHd1FwbDTeR1uxrje1ecBMUIRxqTqMZ0oE7aYG-ZTPWHBGIX7x1BPCHRAXUb5vpDz0hoTI1e-tD9IcZ9D7qp6NLxliEBMZz8HvtBSiXzPYniiDWz_5eEj2id95htyaeN8QhDZquLRuAPcF-yWiRrkgh7QpFlxZvv5VsmArnXFgto8eFjsN0bmKRDPRAIAIkU1RedkvKEURFfReKrD_81N4NeczsVnubZ1NPb4G_CzzvjD11cGDKF6XO39zEGI6g-mqqWW8au7eQWcm0oeiL1vr6Iec8ZkSZC9YFglAnwGKPrxTQoEVv7TETZq3SrWNS9JicrEnKZJkBGGEphj_G3_ESrWfD7VYAH_ExRb-iDYEsPjWHNEhhjpUSpyHwf7trfJ8NVFBFeOo40m3ChpXr8RK-nYxAkiqOPHwDaZhjq-0eRuuQwejSrXPISUsPkZpQ=w1848-h952-ft',
    contextualSaveBarSource:
      'https://avada-popups.firebaseapp.com/images/AvadaLogo-back.png?b24ff4e9b7bedd9c1ec9046df5a366be',
    url: '/',
    accessibilityLabel: 'AVADA Sample App'
  }
};

/**
 * The main endpoint of application contains all routes, settings for redux and Polaris
 *
 * @return {React.FunctionComponent}
 * @constructor
 */
export default function App() {
  return (
    <AppProvider
      i18n={translations}
      theme={theme}
      linkComponent={ReactRouterLink}
    >
      <Router history={history}>
        <AppLayout>
          <ErrorBoundary>
            <Routes />
          </ErrorBoundary>
        </AppLayout>
      </Router>
    </AppProvider>
  );
}
