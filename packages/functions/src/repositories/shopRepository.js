import {Firestore} from '@google-cloud/firestore';

const firestore = new Firestore();

/**
 * @documentation
 *
 * Only use one repository to connect to one collection, do not
 * try to connect more than one collection from one repository
 */

const shopRef = firestore.collection('shops');

/**
 *
 * @param {*} shopifyDomain
 * @returns
 */
async function getShopByShopifyDomain(shopifyDomain) {
  const snapshot = await shopRef
    .where('shopifyDomain', '==', shopifyDomain)
    .get();
  if (snapshot.empty) {
    console.log('No matching documents.');
    return;
  }
  let data;
  snapshot.docs.map(doc => {
    data = {...doc.data(), id: doc.id};
  });
  return data;
}

module.exports = {
  getShopByShopifyDomain
};
