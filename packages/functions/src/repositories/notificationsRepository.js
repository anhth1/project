import {Firestore} from '@google-cloud/firestore';
import moment from 'moment';
import * as shopifyApiService from '../services/shopifyApiService';

const firestore = new Firestore();
/**
 * @documentation
 *
 * Only use one repository to connect to one collection, do not
 * try to connect more than one collection from one repository
 */

const notificationsRef = firestore.collection('notifications');
/**
 *
 * @param {*} id
 * @returns
 */
async function getNotificationByShopId({id, sort = 'desc', page}) {
  const refNotifications = notificationsRef
    .where('shopID', '==', id)
    .orderBy('timestamp', sort);
  const snapshot = await refNotifications.get();
  const last = snapshot.docs[snapshot.docs.length - 1];
  // console.log(last);
  // const next = await refNotifications
  //   .startAfter(last.data().timestamp)
  //   .limit(5)
  //   .get();
  if (snapshot.empty) {
    console.log('No matching documents.');
    return;
  }
  let data = [];
  if (page === '1') {
    snapshot.docs.map(doc => {
      data.push({
        ...doc.data(),
        timestamp: doc.data().timestamp.toDate()
      });
    });
  } else {
    next.docs.map(doc => {
      data.push({
        ...doc.data(),
        timestamp: doc.data().timestamp.toDate()
      });
    });
  }

  return data;
}
/**
 *
 * @param {*} param0
 */
async function getNotification({shopID, shopifyDomain, orders, shopify}) {
  var productIds = orders.map(order => {
    return order.line_items[0].product_id;
  });

  productIds = [...new Set(productIds)];

  const products = await shopifyApiService.getProductsList(
    shopify,
    productIds.toString()
  ); //todo: sua lai
  // console.log(products);
  // Sua lai big O n2
  await Promise.all(
    orders.map(order => {
      const productId = order.line_items[0].product_id;
      const product = products.find(product => {
        return product.id === productId;
      });

      notificationsRef.add({
        firstName: order.billing_address.first_name,
        city: order.billing_address.city,
        productName: product.title,
        country: order.billing_address.country,
        productId: productId,
        timestamp: new Date(order.created_at),
        productImage: product.image.src,
        shopID: shopID,
        shopifyDomain,
        productLink: `https://${shopifyDomain}/products/${product.handle}`
      });
    })
  );
}

/**
 *
 * @param {*} shopify
 * @param {*} orderData
 * @returns
 */
async function getNotificationItem(shopify, orderData, shopifyDomain) {
  const productId = orderData.line_items[0].product_id;
  const product = await shopify.product.get(productId);
  return {
    firstName: orderData.billing_address.first_name,
    city: orderData.billing_address.city,
    productName: product.title,
    country: orderData.billing_address.country,
    productId: productId,
    timestamp: new Date(orderData.created_at),
    productImage: product.image.src,
    productLink: `https://${shopifyDomain}/products/${product.handle}`
  };
}

/**
 *
 * @param {*} param0
 */

async function addNotification({shopID, shopifyDomain, data}) {
  await notificationsRef.add({
    ...data,
    shopID: shopID,
    shopifyDomain: shopifyDomain
  });
  return;
}

/**
 *
 * @param {*} shopifyDomain
 * @returns
 */

async function getNotificationByShopifyDomain(shopifyDomain) {
  const snapshot = await notificationsRef
    .where('shopifyDomain', '==', shopifyDomain)
    .orderBy('timestamp', 'desc')
    .get();
  if (snapshot.empty) {
    console.log('No matching documents.');
    return;
  }
  let data = snapshot.docs.map(doc => {
    const {shopID, ...pickerShop} = doc.data();

    return {
      ...pickerShop,
      timestamp: pickerShop.timestamp.toDate(),
      relativeDate: moment(pickerShop.timestamp.toDate()).fromNow()
    };
  });
  return data;
}

module.exports = {
  getNotificationByShopId,
  getNotification,
  addNotification,
  getNotificationItem,
  getNotificationByShopifyDomain
};
