import {Firestore} from '@google-cloud/firestore';
import defaultSetting from '../constants/defaultSettings';
import {replaceUrls} from '../helpers/helpers';

const firestore = new Firestore();
/**
 * @documentation
 *
 * Only use one repository to connect to one collection, do not
 * try to connect more than one collection from one repository
 */
const settingsRef = firestore.collection('settings');

/**
 *
 * @param {*} id
 * @returns
 */
async function getSettingsByShopId(id) {
  const snapshot = await settingsRef.where('shopID', '==', id).get();
  if (snapshot.empty) {
    console.log('No matching documents.');
    return;
  }
  let data;
  snapshot.docs.map(doc => {
    data = doc.data();
  });
  return data;
}

/**
 *
 * @param {*} id
 * @param {*} settingUpdate
 * @returns
 */

async function updateSettings(id, settingUpdate) {
  const snapshot = await settingsRef.where('shopID', '==', id).get();
  if (snapshot.empty) {
    console.log('No matching documents.');
    return;
  }
  snapshot.docs.map(doc => {
    doc.ref.update(settingUpdate);
  });
}

/**
 *
 * @param {*} id
 */

async function setDefaultSettings(shopID, shopifyDomain) {
  await settingsRef.add({
    ...defaultSetting,
    shopID: shopID,
    shopifyDomain: shopifyDomain
  });
}

/**
 *
 * @param {*} shopifyDomain
 * @returns
 */
async function getSettingsByShopifyDomain(shopifyDomain) {
  const snapshot = await settingsRef
    .where('shopifyDomain', '==', shopifyDomain)
    .get();
  if (snapshot.empty) {
    console.log('No matching documents.');
    return;
  }
  let data;
  snapshot.docs.map(doc => {
    const {shopID, ...pickerShop} = doc.data();
    data = {
      ...pickerShop,
      excludedUrls: replaceUrls(pickerShop.excludedUrls),
      includedUrls: replaceUrls(pickerShop.includedUrls)
    };
    delete data.shopID;
  });
  return data;
}

module.exports = {
  getSettingsByShopId,
  updateSettings,
  setDefaultSettings,
  getSettingsByShopifyDomain
};
