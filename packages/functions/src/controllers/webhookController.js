import {getShopByShopifyDomain} from '../repositories/shopRepository';
import {
  addNotification,
  getNotificationItem
} from '../repositories/notificationsRepository';
const Shopify = require('shopify-api-node');

async function listenNewOrder(ctx) {
  try {
    const shopifyDomain = ctx.get('X-Shopify-Shop-Domain');

    const orderData = ctx.req.body;

    const shop = await getShopByShopifyDomain(shopifyDomain);
    const shopify = new Shopify({
      shopName: shop.name,
      accessToken: shop.accessToken
    });
    const notification = await getNotificationItem(
      shopify,
      orderData,
      shopifyDomain
    );
    await addNotification({
      shopID: shop.id,
      shopifyDomain: shopifyDomain,
      data: notification
    });
    return (ctx.body = {
      success: true
    });
  } catch (error) {
    console.error(error);
    return (ctx.body = {
      success: false
    });
  }
}

module.exports = {
  listenNewOrder
};
