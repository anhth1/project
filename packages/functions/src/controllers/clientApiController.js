import {getNotificationByShopifyDomain} from '../repositories/notificationsRepository';
import {getSettingsByShopifyDomain} from '../repositories/settingsRepository';
/**
 *
 * @param {*} ctx
 * @returns
 */
export async function listNotification(ctx) {
  try {
    const {shopifyDomain} = ctx.query;
    const notification = getNotificationByShopifyDomain(shopifyDomain);
    const settings = getSettingsByShopifyDomain(shopifyDomain);
    await Promise.all([notification, settings]).then(
      ([notificationData, settingsData]) => {
        return (ctx.body = {
          notifications: notificationData,
          settings: settingsData
        });
      }
    );
  } catch (error) {
    console.log(error);
    return (ctx.body = {
      success: false,
      data: []
    });
  }
}
