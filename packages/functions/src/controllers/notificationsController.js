import {getCurrentUserInstance} from '../helpers/auth';
import {getNotificationByShopId} from '../repositories/notificationsRepository';

async function getNoti(ctx) {
  try {
    const {shopID} = getCurrentUserInstance(ctx);
    const sort = ctx.query.sort;
    const page = ctx.query.page;
    console.log(page);
    const notifications = await getNotificationByShopId({
      id: shopID,
      sort,
      page
    });

    return (ctx.body = {
      data: notifications,
      success: true
    });
  } catch (error) {
    ctx.status = 404;
    console.error(error);
    return (ctx.body = {
      data: {},
      success: false
    });
  }
}

module.exports = {
  getNoti
};
