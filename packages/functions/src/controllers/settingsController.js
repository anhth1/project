import {getShop} from '@avada/shopify-auth/build/authentication';
import {getCurrentUserInstance} from '../helpers/auth';
import * as shopifyApiService from '../services/shopifyApiService';
const {
  getSettingsByShopId,
  updateSettings: update
} = require('../repositories/settingsRepository');
const Shopify = require('shopify-api-node');

async function getSetting(ctx) {
  try {
    const {shopID} = getCurrentUserInstance(ctx);
    const setting = await getSettingsByShopId(shopID);
    return (ctx.body = {
      data: setting,
      success: true
    });
  } catch (error) {
    ctx.status = 404;
    console.error(error);
    return (ctx.body = {
      data: {},
      success: false
    });
  }
}

async function updateSettings(ctx) {
  try {
    const {shopID} = await getCurrentUserInstance(ctx);
    const shop = await getShop(ctx);
    const updateData = ctx.req.body;

    const shopify = new Shopify({
      accessToken: shop.accessToken,
      shopName: shop.name
    });

    await Promise.all([
      update(shopID, updateData),
      shopifyApiService.createAssets(shopify, updateData.active)
    ]);
    return (ctx.body = {
      data: updateData,
      success: true
    });
  } catch (error) {
    ctx.status = 404;
    return (ctx.body = {
      data: {},
      success: false
    });
  }
}

module.exports = {
  getSetting,
  updateSettings
};
