export function replaceUrls(urls) {
  return urls.split('\n');
}

export function sortNewest(data) {
  data.sort(function(a, b) {
    return b.timestamp - a.timestamp;
  });
}

export function sortOldest(data) {
  data.sort(function(a, b) {
    return a.timestamp - b.timestamp;
  });
}
