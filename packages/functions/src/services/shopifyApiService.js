export async function createWebhook(shopify) {
  try {
    const webhook = await shopify.webhook.create({
      topic: `orders\/create`,
      address: 'https://fde7-123-16-32-226.ngrok.io/webhook/order/new',
      format: 'json'
    });
    return webhook;
  } catch (error) {
    console.log(error);
  }
}
/**
 *
 * @param {*} param0
 */
export async function createScriptTag(shopify) {
  try {
    const scriptTag = await shopify.scriptTag.create({
      event: 'onload',
      src: 'https://localhost:3000/scripttag/avada-sale-pop.min.js'
    });
    // const scriptTag = await shopify.scriptTag.list();
    // const scriptTag = await shopify.scriptTag.delete(183153393817);
    console.log(scriptTag);
    return scriptTag;
  } catch (error) {
    console.log(error);
  }
}
/**
 *
 * @param {*} shopify
 * @returns
 */
export async function getOrderList(shopify) {
  try {
    return await shopify.order.list({limit: 30});
  } catch (error) {
    console.log(error);
  }
}
/**
 *
 * @param {*} shopify
 * @param {*} params
 * @returns
 */
export async function getProductsList(shopify, params) {
  try {
    return await shopify.product.list({ids: params});
  } catch (error) {
    console.log(error);
  }
}

async function getMainThemeId(shopify) {
  const themes = await shopify.theme.list({fields: 'id, role'});
  const mainTheme = themes.filter(theme => theme.role === 'main')[0];

  return mainTheme.id;
}

export async function createAssets(shopify, status = null) {
  const themeId = await getMainThemeId(shopify);
  const theme = await shopify.asset.get(themeId, {
    'asset[key]': 'layout/theme.liquid',
    theme_id: themeId
  });

  console.log(status);
  let themeValue = theme.value;
  if (themeValue.indexOf(SETTING_SNIPPET_VALUE) < 0) {
    const position = themeValue.indexOf('<title>');
    const value = insertSnippet(
      themeValue,
      `\n${SETTING_SNIPPET_VALUE}\n`,
      position
    );

    await shopify.asset.update(themeId, {
      key: 'layout/theme.liquid',
      value
    });
  }

  const updateJobs = [];
  if (status !== null) {
    // Insert assets
    updateJobs.push(
      shopify.asset.create(themeId, {
        key: APP_STATUS_SNIPPET,
        value: `{% assign avadaSpStatus = ${status} %}`
      })
    );
  }

  updateJobs.push(
    shopify.asset.create(themeId, {
      key: SETTING_SNIPPET,
      value: SETTING_SNIPPET_SCRIPT
    })
  );

  await Promise.all(updateJobs);
}

/**
 * App status variable snippet
 *
 * @type {string}
 */
const APP_STATUS_SNIPPET = 'snippets/avada-sp-status.liquid';

const SETTING_SNIPPET_SCRIPT = `
{% include 'avada-sp-status' %}
{% assign scripttags = content_for_header | split: 'var urls = ["' %}
{% if scripttags.size > 1 %}
  {% assign scripttags = scripttags[1] %}
  {% assign scripttags = scripttags | split: '"];' %}
  {% if scripttags.size > 1 %}
    {% assign scripttags = scripttags[0] %}
    {% assign scripttags = scripttags | split: '","' %}
  {% endif %}
{% endif %}
{% assign avada_sp_scriptag = "" %}
{% for url in scripttags %}
  {% if url contains "avada-sale-pop.min.js"%}
    {% assign avada_sp_scripttag = url %}
  {% endif %}
{% endfor %}
{% if avada_sp_scripttag != "" and avadaSpStatus == false %}
  {% assign content_for_header = content_for_header | remove: avada_sp_scripttag %}
{% endif %}
`;

/**
 * Snippet insert to the Shopify theme
 *
 * @type {string}
 */
const SETTING_SNIPPET_VALUE = `
  <!-- Avada Sale Pop Script -->
 {% include 'avada-sp-setting' %}
  <!-- /Avada Sale Pop Script -->
`;

/**
 * Name of the snippet that contains sales pop app status snippet attached to user's theme
 *
 * @type {string}
 */
export const SETTING_SNIPPET = 'snippets/avada-sp-setting.liquid';

function insertSnippet(theme, snippet, position) {
  return [theme.slice(0, position), snippet, theme.slice(position)].join('');
}
