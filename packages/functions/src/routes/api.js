import Router from 'koa-router';
import * as sampleController from '../controllers/sampleController';
import * as settingsController from '../controllers/settingsController';
import * as notificationsController from '../controllers/notificationsController';
import {verifyRequest} from '@avada/shopify-auth';

const router = new Router({
  prefix: '/api'
});

router.use(verifyRequest());

router.get('/samples', sampleController.exampleAction);
router.get('/settings', settingsController.getSetting);
router.put('/settings', settingsController.updateSettings);
router.get('/notifications', notificationsController.getNoti);

export default router;
