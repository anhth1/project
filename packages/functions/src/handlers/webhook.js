import App from 'koa';
import * as errorService from '../services/errorService';
import router from '../routes/webhook';
import cors from 'koa2-cors';

// Initialize all demand configuration for an application
const webhook = new App();
webhook.proxy = true;

// Register all routes for the application
webhook.use(cors());
webhook.use(router.allowedMethods());
webhook.use(router.routes());

// Handling all errors
webhook.on('error', errorService.handleError);

export default webhook;
