import App from 'koa';
import 'isomorphic-fetch';
import {shopifyAuth} from '@avada/shopify-auth';
import {setDefaultSettings} from '../repositories/settingsRepository';
import shopifyConfig from '../config/shopify';
import render from 'koa-ejs';
import path from 'path';
import createErrorHandler from '../middleware/errorHandler';
import firebase from 'firebase-admin';
import * as errorService from '../services/errorService';
import api from './api';
import * as shopifyApiService from '../services/shopifyApiService';
import {getNotification} from '../repositories/notificationsRepository';
import {getShopByShopifyDomain} from '../repositories/shopRepository';
const Shopify = require('shopify-api-node');

if (firebase.apps.length === 0) {
  firebase.initializeApp();
}

// Initialize all demand configuration for an application
const app = new App();
app.proxy = true;

render(app, {
  cache: true,
  debug: false,
  layout: false,
  root: path.resolve(__dirname, '../../views'),
  viewExt: 'html'
});
app.use(createErrorHandler());

// Register all routes for the application
app.use(
  shopifyAuth({
    apiKey: shopifyConfig.apiKey,
    firebaseApiKey: shopifyConfig.firebaseApiKey,
    initialPlan: {
      features: {},
      id: 'free',
      name: 'Free plan',
      periodDays: 3650,
      price: 0,
      trialDays: 0
    },
    scopes: shopifyConfig.scopes,
    secret: shopifyConfig.secret,
    successRedirect: '/',
    afterInstall: async ctx => {
      try {
        const shopifyDomain = ctx.state.shopify.shop;
        const shop = await getShopByShopifyDomain(shopifyDomain);
        const shopify = new Shopify({
          accessToken: shop.accessToken,
          shopName: shop.name
        });
        const orders = await shopifyApiService.getOrderList(shopify);
        await Promise.all([
          setDefaultSettings(shop.id, shopifyDomain),
          getNotification({
            shopID: shop.id,
            shopifyDomain: shopifyDomain,
            orders: orders,
            shopify: shopify
          }),
          shopifyApiService.createAssets(shopify, false),
          shopifyApiService.createWebhook(shopify),
          shopifyApiService.createScriptTag(shopify)
        ]);
        return;
      } catch (error) {
        console.log(error);
      }
    }
  }).routes()
);

// Handling all errors
api.on('error', errorService.handleError);

export default app;
