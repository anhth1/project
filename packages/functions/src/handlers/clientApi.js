import App from 'koa';
import * as errorService from '../services/errorService';
import router from '../routes/clientApi';
import cors from 'koa2-cors';

// Initialize all demand configuration for an application
const clientApi = new App();

// Register all routes for the application
clientApi.use(cors());
clientApi.use(router.allowedMethods());
clientApi.use(router.routes());

// Handling all errors
clientApi.on('error', errorService.handleError);

export default clientApi;
